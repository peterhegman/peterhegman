## About me

:wave: Hi I am Peter Hegman, a Senior Frontend Engineer on the [Organizations](https://handbook.gitlab.com/handbook/engineering/infrastructure-platforms/tenant-scale/organizations/) team at GitLab. I live in [Bellingham WA, USA](https://maps.app.goo.gl/vnvDetyufXaqVVaU6) and enjoy exploring the outdoors by bike, foot, and skis in my free time. I enjoy working with many different languages and frameworks but primarily focus on JavaScript and Vue.

## How I work

- I generally start my day around 7:30am PST.
- I check my To-Dos to help prioritize my day.
- I review MRs that are assigned to me.
- I respond to feedback on MRs authored by me.
- I respond to pings and help refine issues.
- I work on planned work for the current milestone.

## Communicating with me

I prefer async communication on issues and MRs but I am always happy to hop on a Zoom call if something is easier to discus face to face. :coffee: Coffee chats are always welcome!
